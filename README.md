# Dictionnaire de mots croisés

Est une simple application, que j'ai créée car je ne trouvais pas de dictionnaire de mots croisés simple et/ou sans publicités/autorisations abusives.

Cette application ne requiert pas de connexion à Internet et peu donc être utilisée partout :)

**Prés-requis**
* Android 4.0 ou supérieur
* 15 Mo d'espace disque environ

## Installation

Importez le projet sous votre IDE préférée, installez une base de données.
Générez l'APK puis copiez-le sur votre appareil.

## Notes sur la base de données

La base de données est extraite du projet GPL [Français - Gutemberg](http://www.fifi.org/doc/ifrench-gut/fr/ALIRE)

Vous pouvez la personaliser en utilisant la structure suivante

```sql
CREATE TABLE `Mots` (
  `_id`	INTEGER NOT NULL DEFAULT (null),
  `Mot`	TEXT NOT NULL,
  `Definition`	TEXT,
  PRIMARY KEY(_id)
);
```

## Utilisation

Pour rechercher les mots, tapez les lettres connues, puis utilisez la touche ``?`` du clavier virtuel pour insérer un caractére à substituer

** Exemple **

trav????e

* Travaille
* Travelage
* Traversee
* ...

## Licence

Cette application est sous licence GPL v2, voir le ficher LICENCE.md pour plus d'information