package com.egs.model;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 20/01/2019
 */

public class Database extends SQLiteOpenHelper {
	
    // database path and name
    private static String DB_PATH = "/data/data/com.ego.dico/databases/";
    private static String DB_NAME = "dicoMots.db";
 
    private SQLiteDatabase database;
 
	private final Context myContext;
	
	// constructor
	public Database(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	}
	
	@Override
	public synchronized void close() {
		if(database != null) {
			database.close();
			super.close();
		}	
	}

	/**
	 * Create empty database file, to be used as placeholder for final database
	 * 
	 * @throws IOException
	 */
	public void createDatabase() throws IOException {
 
		boolean databaseAlreadyExists = checkForExistingDatabase();
		
		if(!databaseAlreadyExists){
			// creating empty database file
			this.getReadableDatabase();
			
			try {
				copyDatabase();
			} catch (IOException e) {
				throw new Error("Error copying database");
			}
		}
	}
	
	/**
	 * Data import process should be used at app first lauch
	 * In order to avoid to relauch the process at next app lauch, let's checks if the database
	 * is alreadty created
	 *
	 * @return boolean true if exists
	 */
	public boolean checkForExistingDatabase() {
		
		SQLiteDatabase checkDB = null;
		
		try {
			String pathBdd = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(pathBdd, null, SQLiteDatabase.OPEN_READONLY);
			
			Log.d("DICO", "Bdd trouvée \\o/");
		} catch(SQLiteException e) {
			Log.d("DICO", "Bdd pas trouvée :( ");
		}
		
		if (checkDB != null) {
			checkDB.close();
		}
		
		return checkDB != null ? true : false;
	}
 
	/**
	 * Copy database content from assets to the empty database file previously created
	 * 
	 * @throws IOException
	 */
	private void copyDatabase() throws IOException {

		// opening file from assets
		InputStream databaseSourceFile = myContext.getAssets().open(DB_NAME);
		
		// path to empty database file
		String databaseDestinationFilePath = DB_PATH + DB_NAME;
		OutputStream databaseDestinationFile = new FileOutputStream(databaseDestinationFilePath);
	
		// buffuring via a byte array
		byte[] buffer = new byte[1024];
	
		int length;
		
		while ((length = databaseSourceFile.read(buffer)) > 0){
			databaseDestinationFile.write(buffer, 0, length);
		}
		
		// closing data streams
		databaseDestinationFile.flush();
		databaseDestinationFile.close();
		databaseSourceFile.close();
	}
	
	/**
	 * Open existing database
	 * 
	 * @throws SQLException
	 */
	public void openDatabase() throws SQLException {
		String databaseFilePath = DB_PATH + DB_NAME;
		database = SQLiteDatabase.openDatabase(databaseFilePath, null, SQLiteDatabase.OPEN_READONLY);
	}

	@Override
	public void onOpen(SQLiteDatabase database) {
		super.onOpen(database);

		// fixing android 9 database issues
		if(Build.VERSION.SDK_INT >= 28) {
			database.disableWriteAheadLogging();
		}
	}
}
