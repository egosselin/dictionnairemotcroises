package com.egs.persister;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.egs.model.Word;

import java.util.ArrayList;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 20/01/2019
 */

public class WordPersister extends DatabasePersister {

	//valeurs par defauts
	private static final String TABLE	 			= "Mots";
	private static final String COL_ID 				= "_id";
	private static final int NUM_COL_ID 			= 0;
	private static final String COL_MOT 			= "Word";
	private static final int NUM_COL_MOT 			= 1;
	private static final String COL_MOT_PLAT 		= "Mot_Plat";
	private static final int NUM_COL_MOT_PLAT 		= 2;
	private static final String COL_DEFINITION 		= "Definition";
	private static final int NUM_COL_DEFINITION 	= 3;
	
	
	/**
	 * Constructor
	 * 
	 * @param context
	 */
	public WordPersister(Context context) {
		super(context);
	}
	
	/**
	 * Insert data
	 * 
 	 * @param word
	 * @return
	 */
	public long insert(Word word){
		return 0;
	}

	/**
	 * Update data
	 * 
	 * @param word
	 * @return
	 */
	public long update(Word word){
		return 0;
	}

	/**
	 * Select words
	 * 
	 * @param wordString
	 * @return	ArrayListe<Word>
	 */
	public ArrayList<Word> findWordsWithString(String wordString){
		
		ArrayList<Word> output = new ArrayList<Word>();
		Boolean validQuery = true;
		
		// 1st test, is string empty
		if (wordString.length() == 0) {
			validQuery = false;
		}
		
		// 2nd test, is string containing illegals words
		if (!wordString.matches("^[a-z?]+$")) {
			validQuery = false;
		}
		
		// if word found
		if (validQuery) {
			Cursor cursor = sqlite.rawQuery(
				"SELECT * "+
				"FROM "+TABLE+" " +
				"WHERE "+COL_MOT_PLAT+" LIKE '" + wordString.replace("?", "_") + "' "+
				"LIMIT 50",
				new String[]{}
			);
			
			if (cursor.getCount() == 0){
				// if no results, retuning empty arrayList
				return output;
			}
			
			while (cursor.moveToNext()) {
				Word word = new Word();
				word.setId(cursor.getInt(NUM_COL_ID));
				word.setWord(cursor.getString(NUM_COL_MOT));
				word.setDefinition(cursor.getString(NUM_COL_DEFINITION));

				output.add(word);
			}
		}
		
		return output;
	}
}
