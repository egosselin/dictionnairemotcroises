package com.egs.listener;

import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.widget.EditText;

import com.egs.MainActivity;
import com.egs.handler.WordLookupFactory;
import com.egs.keyboard.Key;
import com.egs.model.Word;
import com.egs.persister.WordPersister;
import java.util.ArrayList;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 19/10/2019
 */

public class CustomKeyboardActionListener implements KeyboardView.OnKeyboardActionListener {

    protected EditText output;
    protected Handler wordSearchHandler;

    public void setOutput(EditText output) {
        this.output = output;
    }

    @Override public void onKey(int primaryCode, int[] keyCodes) {
        Key key = new Key();

        // processing special keycodes
        if (primaryCode == 28) {
            // delete button
            Editable editable = this.output.getText();
            int charCount = this.output.getSelectionEnd();

            if (charCount > 0) {
                editable.delete(charCount - 1, charCount);
            }
        } else if (primaryCode == 29) {
            // search button

            WordLookupFactory handlerFactory = new WordLookupFactory();
            wordSearchHandler = handlerFactory.creer(Looper.getMainLooper(), MainActivity.activity);

            MainActivity.openMessage("Requête", "Recherche en cours...");

            // launch search in a thread
            Thread wordSearchThread = new Thread(new Runnable() {
                Message message;
                Bundle messageBundle = new Bundle();

                @Override
                public void run() {
                    WordPersister wordsTable = new WordPersister(MainActivity.activity);

                    wordsTable.open();
                    ArrayList<Word> resultats = wordsTable.findWordsWithString(output.getText().toString());
                    wordsTable.close();

                    // send results to handler
                    message = wordSearchHandler.obtainMessage();
                    messageBundle.putSerializable("words", resultats);
                    message.setData(messageBundle);
                    wordSearchHandler.sendMessage(message);

                    // closing in-progress message
                    MainActivity.closeMessage();
                }
            });

            // starting thread
            wordSearchThread.start();
        } else {
            // append char to view
            String letter = key.getKeyStringFromKeyCode(primaryCode);
            int start = output.getSelectionStart();
            output.getText().insert(start, letter);
        }
    }

    @Override public void onPress(int arg0) {
    }

    @Override public void onRelease(int primaryCode) {
    }

    @Override public void onText(CharSequence text) {
    }

    @Override public void swipeDown() {
    }

    @Override public void swipeLeft() {
    }

    @Override public void swipeRight() {
    }

    @Override public void swipeUp() {
    }
}
