package com.egs.keyboard;

import java.security.InvalidParameterException;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 19/10/2019
 */

public class KeyException extends InvalidParameterException {
    public KeyException(String message){
        super(message);
    }
}
