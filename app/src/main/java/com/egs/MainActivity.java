package com.egs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.egs.dico.R;
import com.egs.listener.CustomKeyboardActionListener;
import com.egs.persister.WordPersister;

/**
 * Ce fichier fait parti du projet dictionnaireMotCroises
 *
 * Voir le fichier README.md pour en savoir plus
 * Ce projet est sous licence GPL v2, voir le fichier LICENCE.md
 *
 * Créé par Elvis Gosselin le 20/01/2019
 */

public class MainActivity extends Activity {

	public static Activity activity;
	public static ProgressDialog progress;
	protected Keyboard customKeyboard;
	protected static KeyboardView customKeyboardView;
	protected EditText textInput;
	protected ListView wordListOutput;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set layout
		setContentView(R.layout.main_activity);
		
		MainActivity.activity = this;
		progress = new ProgressDialog(this);

		// bind custom keyboard to text area
		this.textInput = findViewById(R.id.textInput);

		this.customKeyboard = new Keyboard(this, R.xml.custom_keyboard);
		this.customKeyboardView = findViewById(R.id.keyboardview);
		this.customKeyboardView.setKeyboard(this.customKeyboard);
		this.customKeyboardView.setPreviewEnabled(false);

		// setting keyboard listener
		CustomKeyboardActionListener keyboarListener = new CustomKeyboardActionListener();
		keyboarListener.setOutput(this.textInput);
		this.customKeyboardView.setOnKeyboardActionListener(keyboarListener);

		// hide default system keyboard
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // for api version 21 or more
            this.textInput.setShowSoftInputOnFocus(false);
        } else {
            // for api version below 21
            this.textInput.setTextIsSelectable(true);
        }

        this.textInput.setTextIsSelectable(true);

		this.textInput.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openKeyboard(v);
			}
		});

		this.wordListOutput = findViewById(R.id.wordListOuput);
	}

    /**
     * Open keyboard in activity
     * See WordLookup handler class for keyboard closing
     *
     * @param v
     */
	public void openKeyboard(View v) {
		customKeyboardView.setVisibility(View.VISIBLE);
		customKeyboardView.setEnabled(true);
	}

	@Override
	protected void onStart() {
		super.onStart();
        
        // notifying user of database initialisation
		MainActivity.openMessage("Initialisation", "Création de la base de données");
        
		// creating database in separate thread
        new Thread((new Runnable() {
            @Override
            public void run() {
            	// initializing dababase
            	WordPersister wordPersister = new WordPersister(MainActivity.activity);
          		wordPersister.open();
          		
          		// closing notification
          		MainActivity.closeMessage();
            }
        })).start();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity, menu);
		return true;
	}
	
	/**
	 * Open a message
	 * 
	 * @param title message title
	 * @param message message content
	 */
	public static void openMessage(String title, String message) {
		progress.setTitle(title);
		progress.setMessage(message);
		progress.setIndeterminate(true);
		progress.show();
	}
	
	/**
	 * Close a message
	 */
	public static void closeMessage() {
		progress.dismiss();
	}
}
